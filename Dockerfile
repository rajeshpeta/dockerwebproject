FROM tomcat
MAINTAINER Rajesh Peta <rajeshpeta67@gmail.com>
RUN rm -rf /usr/local/tomcat/webapps/*
COPY target/DockerWebProject-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ROOT.war
EXPOSE 8080
CMD ["catalina.sh", "run"]